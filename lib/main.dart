import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../helpers/constants.dart';
import './pages/pages.dart';
import './models/models.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(create: (_) => Settings()),
        ChangeNotifierProvider(create: (_) => Favorites(favorites: [])),
      ],
      child: MaterialApp(
        title: 'DeliMeals',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          colorScheme: ThemeData().colorScheme.copyWith(
                secondary: Colors.amber,
                primary: Colors.red,
              ),
          canvasColor: const Color.fromRGBO(255, 254, 229, 1),
          textTheme: ThemeData.light().textTheme.copyWith(
                headline1: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  fontFamily: 'RobotoCondensed',
                ),
                headline2: const TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  fontFamily: 'Raleway',
                ),
              ),
        ),
        routes: {
          homePage: (context) => const BottomTabPage(),
          categoryMealsPage: (context) => const CategoriesMealsPage(),
          recipePage: (context) => const RecipePage(),
          configurationsPage: (context) => const ConfigurationsPage(),
        },
        onUnknownRoute: (settings) => MaterialPageRoute(
          builder: (context) => const MyHomePage(),
        ),
      ),
    );
  }
}
