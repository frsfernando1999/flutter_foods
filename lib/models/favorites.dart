import 'package:flutter/material.dart';

class Favorites with ChangeNotifier {
  final List<String> favorites;

  Favorites({
    required this.favorites,
  });

  void toggleFavorite(String favorite) {
    if (favorites.contains(favorite)) {
      favorites.remove(favorite);
    } else {
      favorites.add(favorite);
    }
    notifyListeners();
  }
}
