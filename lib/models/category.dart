import 'package:flutter/material.dart';

class CategoryName {
  final String id;
  final String title;
  final MaterialColor color;

  const CategoryName({
    required this.id,
    required this.title,
    required this.color,
  });
}
