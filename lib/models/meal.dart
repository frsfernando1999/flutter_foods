enum Cost { cheap, fair, expensive }

enum Complexity { simple, medium, hard }

extension ComplexityDescription on Complexity {
  String get getDescription {
    switch (this) {
      case Complexity.simple:
        return 'Simples';
      case Complexity.medium:
        return 'Médio';
      case Complexity.hard:
        return 'Dificil';
    }
  }
}

extension CostDescription on Cost {
  String get getCost {
    switch (this) {
      case Cost.cheap:
        return 'Barato';
      case Cost.fair:
        return 'Justo';
      case Cost.expensive:
        return 'Caro';
    }
  }
}

class Meal {
  final String id;
  final List<String> categories;
  final String title;
  final Cost cost;
  final Complexity complexity;
  final String imageUrl;
  final int duration;
  final List<String> ingredients;
  final List<String> steps;
  final bool isGlutenFree;
  final bool isVegan;
  final bool isVegetarian;
  final bool isLactoseFree;

  const Meal({
    required this.id,
    required this.categories,
    required this.title,
    required this.cost,
    required this.complexity,
    required this.imageUrl,
    required this.duration,
    required this.ingredients,
    required this.steps,
    required this.isGlutenFree,
    required this.isVegan,
    required this.isVegetarian,
    required this.isLactoseFree,
  });
}
