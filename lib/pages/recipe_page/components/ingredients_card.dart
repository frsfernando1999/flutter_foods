import 'package:flutter/material.dart';

class IngredientsCard extends StatelessWidget {
  const IngredientsCard({required this.ingredient, Key? key})
      : super(key: key);

  final String ingredient;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(ingredient, textAlign: TextAlign.center,),
        ],
      ),
    );
  }
}
