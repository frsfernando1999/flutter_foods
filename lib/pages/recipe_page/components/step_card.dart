import 'package:flutter/material.dart';

class StepCard extends StatelessWidget {
  const StepCard({required this.step, required this.numberOfSteps, Key? key}) : super(key: key);

  final String step;
  final int numberOfSteps;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                CircleAvatar(
                  child: Text('#$numberOfSteps'),
                ),
                SizedBox(width: 12),
                Flexible(child: Text(step)),
              ],
            ),
          )
        ],
      ),
    );
  }
}
