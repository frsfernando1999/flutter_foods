import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../models/models.dart';
import 'components/components.dart';

class RecipePage extends StatelessWidget {
  const RecipePage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Favorites favorites = context.watch<Favorites>();
    final meal = ModalRoute.of(context)?.settings.arguments as Meal;
    return Scaffold(
      appBar: AppBar(
        title: Text(meal.title),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {
          favorites.toggleFavorite(meal.id),
        },
        child: favorites.favorites.contains(meal.id)
            ? const Icon(Icons.star)
            : const Icon(Icons.star_border),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Image.network(meal.imageUrl),
            const SizedBox(height: 12),
            Text(
              meal.title,
              style: Theme.of(context).textTheme.headline1,
            ),
            const Divider(
              height: 2,
              color: Colors.black,
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 8),
              child: Card(
                elevation: 5,
                color: const Color.fromRGBO(255, 255, 113, 1),
                child: Column(
                  children: meal.ingredients
                      .map((ingredient) =>
                          IngredientsCard(ingredient: ingredient))
                      .toList(),
                ),
              ),
            ),
            const Divider(
              height: 2,
              color: Colors.black,
            ),
            const SizedBox(height: 8),
            Text(
              'Recipe Steps',
              style: Theme.of(context).textTheme.headline1,
            ),
            Padding(
              padding: const EdgeInsets.all(8),
              child: Column(
                children: meal.steps
                    .map((step) => StepCard(
                        step: step,
                        numberOfSteps: meal.steps
                                .indexWhere((stepNum) => stepNum == step) +
                            1))
                    .toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
