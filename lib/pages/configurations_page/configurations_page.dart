import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../models/settings.dart';

class ConfigurationsPage extends StatefulWidget {
  const ConfigurationsPage({Key? key}) : super(key: key);

  @override
  State<ConfigurationsPage> createState() => _ConfigurationsPageState();
}

class _ConfigurationsPageState extends State<ConfigurationsPage> {
  @override
  Widget build(BuildContext context) {
    Settings settings = context.read<Settings>();
    Widget _createSwitch({
      required String title,
      required String subtitle,
      required bool value,
      required void Function(bool) onChanged,
    }) {
      return SwitchListTile.adaptive(
        title: Text(title),
        subtitle: Text(subtitle),
        value: value,
        onChanged: onChanged,
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Configurações'),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Filtros',
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline1,
            ),
            _createSwitch(
              title: 'Sem Gluten',
              subtitle: 'Exibir apenas refeições sem glutem',
              value: settings.isGlutenFree,
              onChanged: (value) =>
                  setState(() => settings.isGlutenFree = value),
            ),
            _createSwitch(
              title: 'Sem Lactose',
              subtitle: 'Exibir apenas refeições sem lactose',
              value: settings.isLactoseFree,
              onChanged: (value) =>
                  setState(() => settings.isLactoseFree = value),
            ),
            _createSwitch(
              title: 'Vegetariano',
              subtitle: 'Exibir apenas refeições vegetarianas',
              value: settings.isVegetarian,
              onChanged: (value) =>
                  setState(() => settings.isVegetarian = value),
            ),
            _createSwitch(
              title: 'Vegano',
              subtitle: 'Exibir apenas refeições Veganas',
              value: settings.isVegan,
              onChanged: (value) => setState(() => settings.isVegan = value),
            ),
          ],
        ),
      ),
    );
  }
}
