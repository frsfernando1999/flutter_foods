import 'package:flutter/material.dart';
import '../pages.dart';

import '../../helpers/constants.dart';

class TabsPage extends StatelessWidget {
  const TabsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        drawer: SafeArea(
          child: Drawer(
            child: Column(
              children: [
                ListTile(
                  onTap: () => _navigateToConfig(context),
                  leading: const Icon(Icons.settings),
                  title: const Text('Configurations'),
                  trailing: const Icon(Icons.keyboard_arrow_right),
                )
              ],
            ),
          ),
        ),
        appBar: AppBar(
          title: const Text('Vamos Cozinhar?'),
          centerTitle: true,
          bottom: const TabBar(
            tabs: [
              Tab(
                icon: Icon(Icons.category),
                text: 'Categorias',
              ),
              Tab(
                icon: Icon(Icons.star),
                text: 'Favoritos',
              ),
            ],
          ),
        ),
        body: const TabBarView(children: [
          MyHomePage(),
          FavoritePage(),
        ]),
      ),
    );
  }

  _navigateToConfig(context) {
    Navigator.pushNamed(context, configurationsPage);
  }
}
