import 'package:flutter/material.dart';
import '../../../helpers/constants.dart';

class CustomDrawer extends StatelessWidget {
  const CustomDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top + 8, bottom: 8),
            color: Theme.of(context).colorScheme.secondary,
            child: Text(
              'Vamos Cozinhar?',
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.w900,
                  color: Theme.of(context).colorScheme.primary),
            ),
          ),
          _createDrawerTile(
            icon: Icons.restaurant,
            page: homePage,
            navigate: _navigate,
            pageName: 'Refeições',
            context: context,
          ),
          _createDrawerTile(
            icon: Icons.settings,
            page: configurationsPage,
            navigate: _navigate,
            pageName: 'Configurações',
            context: context,
          ),
        ],
      ),
    );
  }

  void _navigate(BuildContext context, String page) {
    page == homePage
        ? Navigator.pushReplacementNamed(context, page)
        : Navigator.pushNamed(context, page);
  }

  Widget _createDrawerTile({
    required IconData icon,
    required String pageName,
    required void Function(BuildContext, String) navigate,
    required String page,
    required BuildContext context,
  }) {
    return ListTile(
      trailing: const Icon(Icons.keyboard_arrow_right),
      title: Text(pageName),
      leading: Icon(icon),
      onTap: () => navigate(context, page),
    );
  }
}
