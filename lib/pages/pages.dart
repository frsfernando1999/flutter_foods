export './category_meals_page/categories_meals_page.dart';
export './home_page/home_page.dart';
export './configurations_page/configurations_page.dart';
export './recipe_page/recipe_page.dart';
export './tabs_page/tabs_page.dart';
export './tabs_page/bottom_tab_page.dart';
export './favorite_page/favorite_page.dart';
