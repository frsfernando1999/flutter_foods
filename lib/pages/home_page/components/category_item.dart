import 'package:flutter/material.dart';

import '../../../helpers/constants.dart';
import '../../../models/category.dart';

class CategoryItem extends StatelessWidget {
  const CategoryItem({required this.category, Key? key}) : super(key: key);

  final CategoryName category;

  void _selectCategory(context) {
    Navigator.of(context).pushNamed(categoryMealsPage, arguments: category);
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(5),
      splashColor: Theme.of(context).colorScheme.primary,
      onTap: () => _selectCategory(context),
      child: Container(
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          gradient: LinearGradient(colors: [
            category.color.withOpacity(0.5),
            category.color,
          ], begin: Alignment.topLeft, end: Alignment.bottomRight),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: const Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Text(
          category.title,
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
    );
  }
}
