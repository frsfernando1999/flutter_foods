import 'package:flutter/material.dart';

import '../../helpers/constants.dart';
import './components/components.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  MyHomePageState createState() => MyHomePageState();
}

class MyHomePageState extends State<MyHomePage> {
  DateTime? currentBackPressTime;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _popConfirm,
        child: const CategoriesGrid(),
      );
  }

  Future<bool> _popConfirm() {
    if(Scaffold.of(context).isDrawerOpen){
      Navigator.of(context).pop();
      return Future.value(false);
    }
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime as DateTime) >
            const Duration(seconds: 2)) {
      currentBackPressTime = now;
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          duration: Duration(seconds: 2),
          content: Text('Aperte voltar novamente para sair'),
        ),
      );
      return Future.value(false);
    }
    return Future.value(true);
  }
}
