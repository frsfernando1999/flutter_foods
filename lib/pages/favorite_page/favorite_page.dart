import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../data/dummy_data.dart';
import '../../models/models.dart';
import '../category_meals_page/components/components.dart';

class FavoritePage extends StatelessWidget {
  const FavoritePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Favorites favorites = context.watch<Favorites>();
    return ListView(
      children: dummyMeals
          .where((meal) => favorites.favorites.contains(meal.id))
          .map((meal) => MealCard(meal: meal))
          .toList(),
    );
  }
}
