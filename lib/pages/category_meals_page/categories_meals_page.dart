import 'package:flutter/material.dart';
import 'package:meals/models/settings.dart';
import 'package:provider/provider.dart';

import '../../data/dummy_data.dart';
import '../../models/category.dart';
import './components/components.dart';

class CategoriesMealsPage extends StatelessWidget {
  const CategoriesMealsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final category = ModalRoute.of(context)?.settings.arguments as CategoryName;
    final filters = context.read<Settings>();
    return Scaffold(
      appBar: AppBar(
        title: Text(category.title),
        centerTitle: true,
      ),
      body: ListView(
        children: dummyMeals
            .where((meal) => meal.categories.contains(category.id))
            .where((meal) => _filter(meal.isVegetarian, filters.isVegetarian))
            .where((meal) => _filter(meal.isVegan, filters.isVegan))
            .where((meal) => _filter(meal.isGlutenFree, filters.isGlutenFree))
            .where((meal) => _filter(meal.isLactoseFree, filters.isLactoseFree))
            .map((meal) => MealCard(meal: meal))
            .toList(),
      ),
    );
  }
  _filter(bool meal, bool filter) => filter ? meal == true : true;
}
