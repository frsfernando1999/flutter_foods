import 'package:flutter/material.dart';
import 'package:meals/helpers/constants.dart';

import './components.dart';
import '../../../models/models.dart';

class MealCard extends StatelessWidget {
  const MealCard({Key? key, required this.meal}) : super(key: key);
  final Meal meal;
  void _navigateToRecipe(context) async {
    await Navigator.of(context)
        .pushNamed(recipePage, arguments: meal);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 12),
      child: Stack(
        children: [
          GestureDetector(
            onTap: () => _navigateToRecipe(context),
            child: Card(
              semanticContainer: true,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8),
                  topRight: Radius.circular(8),
                ),
              ),
              elevation: 5,
              child: Column(
                children: [
                  Image.network(
                    meal.imageUrl,
                    fit: BoxFit.fill,
                  ),
                  Container(
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 8,
                      ),
                      child: RecipeTags(meal: meal),
                    ),
                  )
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 50,
            right: 4,
            child: Container(
              constraints: BoxConstraints(
                  maxWidth: MediaQuery.of(context).size.width * 0.8),
              padding: const EdgeInsets.all(8),
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8),
                  bottomLeft: Radius.circular(8),
                ),
                color: Colors.black54,
              ),
              child: Text(
                overflow: TextOverflow.clip,
                meal.title,
                style: Theme.of(context).textTheme.headline2,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
