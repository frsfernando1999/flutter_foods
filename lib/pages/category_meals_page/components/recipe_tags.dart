import 'package:flutter/material.dart';

import '../../../models/models.dart';

class RecipeTags extends StatelessWidget {
  const RecipeTags({
    Key? key,
    required this.meal,
  }) : super(key: key);

  final Meal meal;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        RichText(
          text: TextSpan(
            children: [
              const WidgetSpan(
                  child: Icon(Icons.timer),
                  alignment: PlaceholderAlignment.middle),
              TextSpan(
                  text: ' ${meal.duration} min',
                  style: Theme.of(context).textTheme.bodyText2)
            ],
          ),
        ),
        RichText(
          text: TextSpan(
            children: [
              const WidgetSpan(
                  child: Icon(Icons.assignment_late),
                  alignment: PlaceholderAlignment.middle),
              TextSpan(
                  text: ' ${meal.complexity.getDescription}',
                  style: Theme.of(context).textTheme.bodyText2)
            ],
          ),
        ),
        RichText(
          text: TextSpan(
            children: [
              const WidgetSpan(
                  child: Icon(Icons.attach_money),
                  alignment: PlaceholderAlignment.middle),
              TextSpan(
                  text: meal.cost.getCost,
                  style: Theme.of(context).textTheme.bodyText2)
            ],
          ),
        ),
      ],
    );
  }
}
